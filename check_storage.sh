#!/bin/bash

sameAppliance="true"
ipRegex="[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+"
ignoreRegex="(Appliance[[:space:]]Type|[[:digit:]]+:[[:digit:]]+:[[:digit:]]+[[:space:]]+up[[:space:]]+[[:digit:]]+|---+|Filesystem)"
partitionFillThreashold=75
input=$(mktemp)

function parse_line(){
		if [[ "$fs" =~ $ipRegex ]]
		then
			ip=$fs
			hostname=$used
		else
			USEP=$(echo $usep|cut -f1 -d'%')
			if [ $USEP -gt $partitionFillThreashold ]
			then
				echo "$ip,$hostname,$fs,$size,$used,$usep,$mountpoint"
			fi
		fi
}

/opt/qradar/support/all_servers.sh -Ck df -h > $input

echo '"IP","Hostname","Filesystem","Size","Used","Used %","Mount point"'
while read fs size used avail usep mountpoint
do
	#echo '"'$fs $size $used $avail $usep $mountpoint'"'
	if [ "$sameAppliance" == "true" ]
	then

		if [ -z "$fs" ] && [ -z  "$size" ] && [ -z  "$used" ]
		then
			sameAppliance="false"
		else
			[[ "$fs $size $used" =~ $ignoreRegex ]] && continue

			parse_line
		fi
	else
		[[ (-z "$fs" &&  -z  "$size" && -z  "$used") ]] && continue
		[[ "$fs $size $used" =~ $ignoreRegex ]] && continue

		sameAppliance="true"

		parse_line
	fi

done < $input


