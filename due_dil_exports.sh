#!/bin/bash

#TODO: Add CLI options
exportsDir=/tmp/due_dil_exports
tarballDir=/tmp
managedHostsFile=/tmp/managed_hosts.txt
numberOfHosts=0
completed=0
hostname=$(uname -n|cut -f1 -d'.')
myIP=$(/opt/qradar/bin/myver -i)
currentDay=$(date +%d%m%Y)
transferDir="/opt/qradar/webapps/console/images/exports"


function qaudit_get_eps_fps () {
	#Get EPS and FPS
	echo "Collecting EPS and FPS data"
	rm -f $exportsDir/eps_*.log
	rm -f $exportsDir/fps_*.log
	for logfile in $(\ls -tr /var/log/qradar.old/qradar.log*);
	do
		\cp /$logfile /tmp;
	done
	for logfile in $(\ls -tr /tmp/qradar.log*); 
	do 
		gunzip -f $logfile; 
		gunzipedName=$(echo $logfile|cut -f 1-3 -d '.');
		grep 'Incoming raw event rate' $gunzipedName | grep '\[ecs-ec\.ecs-ec\]' >> $exportsDir/eps_$hostname.log; 
		grep 'Incoming flow rate' $gunzipedName | grep '\[ecs-ec\.ecs-ec\]' >> $exportsDir/fps_$hostname.log; 
	done
	grep 'Incoming raw event rate' /var/log/qradar.log | grep '\[ecs-ec\.ecs-ec\]' >> $exportsDir/eps_$hostname.log
	grep 'Incoming flow rate' /var/log/qradar.log | grep '\[ecs-ec\.ecs-ec\]' >> $exportsDir/fps_$hostname.log

	rm -f /tmp/qradar.log*
}

function qaudit_get_efps_licenses(){
	echo "Collecting EPS and FPM licenses information"
	psql -F ',' --no-align -t -U qradar -c "SELECT serverhost.hostname,allocated_eps_rate,allocated_fps_rate FROM (serverhost LEFT JOIN managedhost ON managedhost.primary_host=serverhost.id OR managedhost.secondary_host=serverhost.id) LEFT JOIN license_pool_allocation ON primary_host=host_id ORDER BY serverhost.hostname;" > licenses.csv
}

function qaudit_get_routes(){
	#Get routes
	echo "Collecting routes"
	route -n > $exportsDir/routes_$hostname.txt
}

function qaudit_get_crontabs() {
	#Get crontabs
	echo "Collecting crontabs for all users"
	for user in $(cat /etc/passwd|cut -f1 -d':'); 
	do 
		crontabContents=$(crontab -l -u $user 2>/dev/null);
		if [ -n "$crontabContents" ]; 
		then 
			echo "$crontabContents" > $exportsDir/crontab_${hostname}_${user}.txt; 
		fi;
	done
}

function qaudit_get_qradar_version(){
	#Get QRadar version
	echo "Collecting version information"
	/opt/qradar/bin/myver -v |sort >> $exportsDir/version_$hostname.txt
}

function qaudit_get_ha_state(){
	#HA state
	echo "Performing HA diagnosis"
	/opt/qradar/support/ha_diagnosis.sh >> $exportsDir/ha_diagnosis_$hostname.txt
}

function qaudit_get_services() {
	#Services
	echo "Collecting service data"
	for service in accumulator.service ariel_proxy_server.service ariel_query_server.service assetprofiler.service dataNode.service docker.service ecs-ec-ingress.service ecs-ec.service ecs-ep.service ha_manager.service historical_correlation_server.service hostcontext.service httpd.service offline_forwarder.service qflow.service qvmadaptor.service qvmhostedscanner.service qvmprocessor.service qvmscanner.service reporting_executor.service tomcat.service; 
	do 
		echo ">>>>>" >> services_$hostname.txt; 
		systemctl status $service >> $exportsDir/services_$hostname.txt; 
	done
}

function qaudit_get_disk_usage() {
	#Partition usage
	echo "Collecting disk usage information"
	df -Th >> $exportsDir/disk_usage_$hostname.txt
}

function qaudit_make_tarball() {
	echo "Creating tarball"
	tarballPath=$tarballDir/due_dil_exports_$hostname.tar.gz
	tar -cvzf $tarballPath $exportsDir
	echo "Tarball can be found at $tarballPath"
}

function qaudit_deployment_info ()
{
	#Deployment information
	echo "Collecting deployment information"
	/opt/qradar/support/deployment_info.sh -A 2>/dev/null
	day=$(date +%m-%d-%Y)
	\mv qradar_deployment_info-$day.csv $exportsDir/deployment_info_$hostname.csv
}
function qaudit_clean(){
	echo "Cleaning export directories"
	rm -rf $exportsDir/*
	rm -rf $tarballDir/due_dil_exports_*.tar.gz
}

function qaudit_run (){
	currentDay=$(date +%d%m%Y)
	mkdir -p $exportsDir 
	echo -e "\x1b[32mExtracting data from $hostname\x1b[0m"
	qaudit_clean
	qaudit_get_eps_fps
	qaudit_get_routes
	qaudit_get_crontabs
	qaudit_get_qradar_version
	qaudit_get_ha_state
	qaudit_get_services
	qaudit_get_disk_usage
	qaudit_deployment_info
	qaudit_make_tarball
}

function qaudit_run_helper(){
	host=$1
	echo -e "\033[1mDeploying on $host\033[0m"
	scp /tmp/due_dil_exports.sh $host:/tmp
	ssh $host "source /tmp/due_dil_exports.sh;qaudit_run"
	scp $host:/tmp/due_*.tar.gz /tmp
}

function qaudit_parse_efps_data(){
	currentDay=$(date +%d%m%Y)
	echo -e "\x1b[1mParsing EPS/FPM data\x1b[0m"
	if [ -e efps_to_csv.py ]
	then
		tar -xvzf data_${currentDay}.tar.gz
		for a in tmp/*.tar.gz
		do
			tar -xvzf $a
		done
		python efps_to_csv.py tmp/due_dil_exports/?ps_*.log
		tar -cvzf efps_data_${currentDay}.tar.gz tmp/due_dil_exports/?ps_*.csv
		mv efps_data_${currentDay}.tar.gz $transferDir/efps_data_${currentDay}
		bash $transferDir/make_listing.sh
	fi
}

function qaudit_deploy(){
	if [ "$(/opt/qradar/bin/myver -c 2>/dev/null)" == "true" ]; 
	then 
		echo "Deploying to managed hosts"
		/opt/qradar/support/all_servers.sh -lkC > $managedHostsFile
		numberOfHosts=$(wc -l $managedHostsFile|awk -F ' ' '{ print $1;}')
		qaudit_get_efps_licenses
	
		for host in $(<$managedHostsFile)
		do
			if [ "$host" == "$myIP" ]
			then
				echo "$host is this machine. Skipping"
			else
				qaudit_run_helper $host&
			fi
		done
		wait
		tar -cvzf data_${currentDay}.tar.gz /tmp/due_*.tar.gz licenses.csv
	fi
}
