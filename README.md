# Introduction

The aim of this project is to automate as much as possible audits of QRadar
platforms. Several scripts are used for that, detailed below.

# extract\_api.py

This script extracts most of the API contents. The endpoints and field extracted
depend on the configuration defined in the file _config.json_

## config.json

JSON configuration file for extract\_api.py. It has the following structure:

```
{
	"host":"<qradar IP or domain>",
	"token":"<API token>",
	"extract_dir":"<target directory to store extracted data>"
	"api_version":"<QRadar API version number to use>"
	"endpoints_to_extract":{
		"<API version>":{
			"<endpoint path>":["<field name>", ...]
			...
		},
		...
	}
	"parametersMappings":{
		"<endpoint>":{
			"<field>":{
				"mappedToEndpoint":"<endpoint the field references>",
				"mappedToParameter":"<corresponding field in the referenced endpoint>",
				"replaceWith":"<replacement field from the referenced endpoint>"
				"filter":"<additional filter to apply>"
			}
		}
	}
}
```
## parse\_json.py

Once data has been extracted with extract\_api.py in JSON format, this script
can be used to convert it to CSV format.

# due\_dil\_exports.sh

This script extracts the following information from the QRadar console and
managed hosts:

- EPS and FPS
- Routes
- Crontabs
- QRadar version
- HA state
- Services
- Disk usage
- Deployment info

