#!/usr/bin/python
from getpass import getpass
from parse_json import JsonParser
import requests
import json
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('--host', type=str, help='URL of QRadar host to connect to')
parser.add_argument('--token', type=str, help='Token to connect to the QRadar API')
parser.add_argument('--user', type=str, help='Username to connect as (ignored if token is provided)')
parser.add_argument('--conf', type=str, help='Configuration file to use', default='config.json')
parser.add_argument('--logstash', action='store_true', help='Format output for Logstash')
parser.add_argument('endpoints', type=str, nargs='*', help='The endpoints to extract')
parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
args = parser.parse_args()

extracted = []
dependencies = {}
host = args.host
token = args.token
user = args.user
password = ''
rangeSize = 10000
extractDir = 'json_extracts'
extractedFiles = {}
headers = { "Accept": "application/json", "SEC":token }
apiVersion = '9.1'
endpointsToExtract = {'analytics/rules':[],'config/event_sources/custom_properties/property_expressions':[],'config/event_sources/custom_properties/regex_properties':[],'config/flow_sources/custom_properties/regex_properties':[],'config/flow_sources/custom_properties/property_expressions':[],'config/network_hierarchy/networks':[],'reference_data/map_of_sets':[],'reference_data/maps':[],'reference_data/sets':[],'reference_data/tables':[],'siem/offenses':[]}
defaultEndpoints = endpointsToExtract.keys()
endpoints = defaultEndpoints
params = {}
conf = {}

def info(message, category='INFO'):
	print('\x1b[38;5;2m[{1}] {0}\x1b[0m'.format(message, category))

def error(message, category='ERROR'):
	print('\x1b[38;5;1m[{1}] {0}\x1b[0m'.format(message, category))

def debug(message, category='DEBUG'):
	if args.verbose:
		print('\x1b[38;5;3m[{1}] {0}\x1b[0m'.format(message, category))

class APIResponse():
	def __init__(self, response, endpoint):
		self.json = json.loads(response.content)
		self.status = response.status_code
		self.endpoint = endpoint
		self.culprit = None
		self.check_status()

	def check_status(self):
		"""
		React to the various status codes returned by the API
		"""
		if self.status == 200:
			debug('200 OK')
		elif self.status >= 400:
			if self.status == 404:
				error('Endpoint {0} not supported by this version of the REST API'.format(self.endpoint.name))
				self.status = None
			elif self.status == 401:
				error('Authentication failure:\n'
				'{0}\n'
				'{1}'.format(self.json['message'],self.json['http_response']))
				exit()
			elif self.status == 422:
				if self.json['code'] == 30:
					m = re.search(r'Specified field "([^\\]+)" is not recognized by this endpoint', self.json['message'])
					if m:
						error('422 Field {0} not recognized by endpoint {1}'.format(m.group(1), self.endpoint.name))
						fields = self.endpoint.params['fields'].split(',')
						fields.remove(m.group(1))
						self.endpoint.set_params(fields=fields)
					else:
						error(self.json['message'])
				elif self.json['code'] == 36:
					error('422 {0}'.format(self.json['message']))
					self.endpoint.set_range()
				elif self.json['code'] == 39:
					if 'Range' in self.endpoint.headers:
						error('422 Paging is not supported by endpoint {0}.'.format(self.endpoint.name))
						self.endpoint.set_range()
						self.endpoint.pagingSupported = False
					elif 'filter' in params:
						error('422 Filtering does not seem to be available for endpoint {0}.'.format(self.endpoint.name))
						self.endpoint.set_params(fields=self.endpoint.params['fields'])
					elif "'fields' is not supported by this endpoint" in self.json['message']:
						error('422 The parameter fields is not supported by endpoint {0}.'.format(self.endpoint.name))
						self.endpoint.set_params(fields=None)
				else:
					error('422 {0}'.format(self.json['message']))
			elif self.status == 500:
				error('500 Internal server error')
				if self.endpoint.pagingSupported:
					error('This might be caused by an invalid element in the range. Trying to find it out...')
					self.culprit = self.endpoint.find_culprit()
			else:
				print(self.json)
				self.status = None
		self.endpoint.tries -= 1

class Endpoint():
	extracted = set() #To keep track of endpoints already extracted
	host = 'localhost'

	def __init__(self, name, headers={'Accept':'application/json'}, params=None):
		self.name = name
		self.params = params
		self.headers = headers
		if token:
			self.headers['SEC'] = token
		self.headers['Allow-Hidden'] = 'true'
		self.pagingSupported = True
		self.filteringSupported = True
		self.fieldSelectionSupported = True
		self.tries = 5

		if 'parametersMappings' in conf:
			mappedFields = conf['parametersMappings'].get(self.name,[])
			mappedTo = list(mappedFields[field]['mappedToEndpoint'] for field in mappedFields)
			self.dependencies = dict.fromkeys(mappedTo,Results())
		else:
			self.dependencies = None
		debug('Dependencies for {0}: {1}'.format(self.name, self.dependencies))
		Endpoint.host = host

	def get_range(self):
		"""
		Get the current range set
		Returns:
		(range min, range max) pair
		"""
		s = self.headers.get('Range', None)
		if s:
			m = re.search(r'items=(\d+)-(\d+)', s)
			if m:
				return (int(m.group(1)), int(m.group(2)))
		return (None, None)

	def set_range(self, low=None, high=None):
		"""
		Set range for the API query
		Parameters:
		low (int): first item to query
		high (int): last item to query
		"""
		if low == None or high == None:
			try:
				del self.headers['Range']
				return
			except KeyError:
				return
		self.headers['Range'] = 'items={0}-{1}'.format(min(low,high), max(low,high))

	def set_params(self, fields=None, filters=None):
		"""
		Set parameters for API queries
		Parameters:
		fields (list): list of fields to get from the API
		filters (str): filter string to pass to the query
		"""
		self.params = {}
		if fields:
			self.params['fields'] = ','.join(fields)
		if filters:
			self.params['filter'] = filters
		if not fields and not filters:
			self.params = None

	def extract(self):
		"""
		Extract data from the endpoint, and write the results to disk
		Produces several files for endpoints with a number of results > rangeSize
		"""
		if self.name in Endpoint.extracted:
			return

		info('Extracting {0}'.format(self.name))
		results = self.get_data()
		while results != Results():

			# Handle dependencies for these results, if any
			if self.dependencies:
				info('Fetching dependencies for {0}'.format(self.name))
				debug(str(self.dependencies))
				self.get_mappings(results)
				results.replace_values(self)
			results.write(self)

			#Paging was unsupported => done extracting
			if 'Range' not in self.headers or len(results) > rangeSize:
				break

			(low, high) = self.get_range()
			self.set_range(high+1, high+rangeSize)
			results = self.get_data()
		Endpoint.extracted.add(self.name)

	def get_mappings(self, results):
		"""
		For endpoints that return fields that reference a field from another
		endpoint, get the interesting value
		Parameters:
		results (list): results from an endpoint
		"""
		mappings = conf['parametersMappings'][self.name]
		mappedFields = set(mappings.keys()).intersection(set(results[0].keys())) # Common fields between what's actually returned in the results and what is configured

		for mappedField in mappedFields:
			targetEndpointResults = Results()
			fieldValues = results.get_field_values(mappedField)
			#try:
				#ranges = results.build_field_ranges(fieldValues)
			ranges = results.build_field_ranges(fieldValues)
			#except TypeError:
				#print(fieldValues[:20], mappedField)
				#exit()
			targetEndpoint = Endpoint(mappings[mappedField]['mappedToEndpoint'])
			targetField = mappings[mappedField]['mappedToParameter']

			filterFormat = '{0}>={1} and {0}<={2}'
			if ranges != []:
				if not type(ranges[0][0]) == int:
					filterFormat = '{0}>="{1}" and {0}<="{2}"'
			for (low, high) in ranges:
				filterStr = filterFormat.format(targetField,low,high)
				targetEndpoint.set_params(filters=filterStr)
				targetEndpointResults += targetEndpoint.get_data()
			self.dependencies[targetEndpoint.name] = targetEndpointResults


	def get_data(self):
		"""
		Send HTTP request to the API, perform some error checking on the response
		Returns: Results object
		"""
		url = 'https://{0}/api/{1}'.format(Endpoint.host, self.name)
		data = Results()

		#Get range for the query
		t = self.get_range()
		(low, high) = (t[0],t[1]) if t else (None, None)
		if (low, high) != (None,None):
			debug('Extracting items {0} to {1}'.format(low, high), 'RANGE')
		else:
			debug('No range defined, extracting everything', 'RANGE')

		#Send query and get response
		debug('Query information'
		'\n\tURL: {0}'
		'\n\tHeaders: {1}'
		'\n\tParameters: {2}'.format(url, self.headers, self.params), 'QUERY')
		response = get(url, headers=self.headers, params=self.params, verify=False)
		#debug(response.text,'RESPONSE')
		response = APIResponse(response, self)
		if response.status == None:
			return data
		elif response.culprit is not None:
			#If an invalid element was found in the range, try to get data
			#again, but without it
			debug('Trying to get data without element {0}. {1} tries left'.format(response.culprit,self.tries))
			if self.tries > 0:
				if response.culprit >= low+1:
					self.set_range(low=low, high=response.culprit - 1)
					data = self.get_data()
				self.set_range(low=response.culprit + 1, high = high)
				data += self.get_data()
				response.culprit = None
				return data
			else:
				error('Max number of retries reached. Giving up')
				return Results()
		elif response.status != 200:
			if self.tries > 0:
				return self.get_data()
			else:
				error('Max number of retries reached. Giving up')
				return Results()

		return Results(response.json)

	def find_culprit(self):
		"""
		Find invalid element in range, because QRadar is not intelligent enough
		to ignore it itself
		Returns:
		Index of invalid element
		"""
		(previousStart, previousEnd) = self.get_range()
		start = previousStart
		end = (previousStart + previousEnd) / 2
		url = 'https://{0}/api/{1}'.format(Endpoint.host, self.name)
		response = get(url, headers=self.headers, params=self.params, verify=False)

		while end > start:
			debug('[{0},{1}]'.format(start, end))
			if response.status_code == 500:
				previousStart = start
				previousEnd = end
				end = (previousStart + previousEnd) / 2
				self.set_range(start, end)
				response = get(url, headers=self.headers, params=self.params, verify=False)
			elif response.status_code == 200:
				previousStart = start
				start = end+1
				end = previousEnd
				self.set_range(start, end)
				response = get(url, headers=self.headers, params=self.params, verify=False)
			else:
				error(response.json()['message'])
		debug('[{0},{1}]'.format(start, end))
		return start

class Results(list):
	"""
	Class to perform some post-treatment on API responses
	"""
	def union(self, l):
		"""
		Build the union of 2 sets of results
		"""
		ret = self
		for bElement in l:
			present = False
			for aElement in self:
				if bElement['id'] == aElement['id']:
					present = True
					break
			if not present:
				ret.append(bElement)
		return ret

	def get_field_values(self,field):
		"""
		Get list of all not null values for a specific field in the results
		Parameters:
		field (str): field name
		Returns:
		list of all not null values for field
		"""
		return list(element[field] for element in self if element[field] != None)

	def replace_values(self, endpoint):
		"""
		Replace the values in the results with the mappings from Enpoint.get_mappings
		parameters:
		endpoint (Endpoint): The endpoint whose the mappings are needed
		"""
		fieldsToReplace = conf['parametersMappings'][endpoint.name]
		for i,element in enumerate(self):
			for field in set(fieldsToReplace).intersection(set(element.keys())):
				targetEndpoint = fieldsToReplace[field]['mappedToEndpoint']
				linkedField = fieldsToReplace[field]['mappedToParameter']
				replacementField = fieldsToReplace[field]['replaceWith']
				linkedFieldValue = element[field]
				for targetElement in endpoint.dependencies[targetEndpoint]:
					try:
						if targetElement[linkedField] == linkedFieldValue:
							self[i][field] = targetElement[replacementField]
							break
					except KeyError:
						# This is when linkedField is not in targetElement. 
						continue

	def build_field_ranges(self,fieldValues):
		"""
		Build ranges for Endpoint.get_mappings to avoid querying entire endpoints
		Parameters:
		fieldValues (list): list of values from results for a specific field
		Returns:
		list of ranges to query
		"""
		rangeList = []
		#fieldValues = map(int,fieldValues)
		if fieldValues:
			fieldValues.sort()
			i = 0
			currentRangeStart = fieldValues[i]
			currentRangeEnd = fieldValues[i]

			while i < len(fieldValues):
				if type(fieldValues[i]) == int:
					if fieldValues[i] - currentRangeStart > 1000:
						rangeList.append((currentRangeStart,currentRangeEnd))
						currentRangeStart = fieldValues[i]
						currentRangeEnd = fieldValues[i]
						i += 1
					else:
						currentRangeEnd = fieldValues[i]
						i += 1
				else:
					currentRangeEnd = fieldValues[i]
					i += 1
			rangeList.append((currentRangeStart,currentRangeEnd))
		return rangeList

	def format_for_logstash(self):
		"""
		Format the JSON to be used by Logstash
		"""
		if self == []:
			return json.dumps(self)
		ret = json.dumps(self[0])
		for elt in self[1:]:
			ret += '\n' + json.dumps(elt)
		return ret

	def write(self, endpoint):
		"""
		Write the results to target file
		"""
		if self == []:
			return
		(low, high) = endpoint.get_range()
		output = '{3}/{0}_{1}_{2}.json'.format(endpoint.name.replace('/','-'), low, high, extractDir)
		with open(output, 'w') as f:
			info('Writing results to {0}'.format(output))
			if args.logstash:
				f.write(self.format_for_logstash())
			else:
				f.write(json.dumps(self))
			extractedFiles[endpoint.name].append(output)

def show_call(decoratedFunction):
	def wrapper(*args, **kwargs):
		log(decoratedFunction.__name__ + '{')
		ret = decoratedFunction(*args, **kwargs)
		log('}')
		return ret
	return wrapper

def log(message):
	with open('extract_api_error.log','a') as log:
		log.write(message + '\n')

def load_conf():
	"""
	Load configuration from configuration file and command line
	"""

	global host, token, headers
	global extractDir, apiVersion, endpoints
	global user, password, endpointsToExtract

	conf = {}
	try:
		with open(args.conf) as f:
			conf = json.load(f)
	except IOError as e:
		error('Could not read configuration file {0}'.format(args.conf))

	host = conf.get('host', 'localhost')
	token = conf.get('token')

	if args.host:
		host = args.host
	if args.token:
		token = args.token
	if args.endpoints:
		endpoints = args.endpoints

	if not token:
		user = 'admin' if not args.user else args.user

	if user:
		password = getpass('Password for user {0}:'.format(user))

	extractDir = conf.get('extract_dir', './')
	apiVersion = conf.get('api_version',apiVersion)

	if 'endpoints_to_extract' in conf:
		if apiVersion in conf['endpoints_to_extract']:
			endpointsToExtract = conf['endpoints_to_extract'][apiVersion]
		else:
			error('The list of endpoints to extract could not be found for version {0} of the API. Falling back to default list: {1}'.format(apiVersion, defaultEndpoints))
	else:
		error('No list of endpoint in configuration file. Falling back to default list: {0}'.format(defaultEndpoints))
	headers['SEC'] = token
	return conf

def get(*args, **kwargs):
	if 'headers' in kwargs and 'SEC' not in kwargs['headers']:
		kwargs['auth']  = (user,password)

	return requests.get(*args, **kwargs)

if __name__=='__main__':
	conf = load_conf()

	info('The following endpoints will be extracted:\n\t{0}\n'.format('\n\t'.join(sorted(endpoints))))

	for endpoint in endpoints:
		extractedFiles[endpoint] = []
		fields = ','.join(endpointsToExtract[endpoint])

		apiEndpoint = Endpoint(endpoint, headers={'Accept':'application/json','Range':'items=0-{0}'.format(rangeSize - 1)}, params={'fields':fields})
		apiEndpoint.extract()

	info('Starting parsing of JSON files')
	for endpoint in endpoints:
		for extractedFile in extractedFiles[endpoint]:
			jp = JsonParser(extractedFile, endpoint, extractedFiles)
			jp.parse()
