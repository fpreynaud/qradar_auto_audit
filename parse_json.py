#!/usr/bin/python
import json
import time

regexPropertyFieldCache = {}
ruleCache = {}

def info(message, category='INFO'):
	print('\x1b[38;5;2m[{1}] {0}\x1b[0m'.format(message, category))

def error(message, category='ERROR'):
	print('\x1b[38;5;1m[{1}] {0}\x1b[0m'.format(message, category))

def debug(message, category='DEBUG'):
	print('\x1b[38;5;3m[{1}] {0}\x1b[0m'.format(message, category))

def timestamp(t):
	timeFormat = '%d %b %Y %H:%M:%S'
	t = t/1000
	tup = time.localtime(t)
	return time.strftime(timeFormat, tup)

class JsonParser:
	"""
	Parse JSON files to convert them to CSV
	"""

	def __init__(self, path, endpoint, extractedFiles={}):
		"""
		path (str): file to convert
		endpoint (str): name of endpoint
		extractedFiles (dict): dictionary mapping filenames produced by extract_api.py to endpoints names
		"""
		self.input = open(path, 'r')
		self.output = path + '.csv' if path[-4:] != '.csv' else path
		self.output = open(self.output, 'w')
		self.results = json.load(self.input)
		self.endpoint = endpoint
		self.extractedFiles = extractedFiles
		self.special = {
			'config/event_sources/custom_properties/property_expressions':self.event_properties,
			'siem/offenses':self.offenses,
		}
		self.replacements = {
			'config/event_sources/custom_properties/property_expressions':{
				'log_source_type_id':'Log Source Type',
				'log_source_id':'Log Source',
				'qid':'QID',
				'low_level_category_id':'Low Level Category'
			},
			'config/event_sources/log_source_management/log_sources':{
				'type_id':'Log Source Type',
				'protocol_type_id':'Protocol Type',
				'target_event_collector_id':'Target Event Collector',
				'wincollect_internal_destination_id':'Wincollect Internal Destination',
				'wincollect_external_destination_ids':'Wincollect Internal Destination IDs'
			},
			'config/event_sources/custom_properties/property_json_expressions':{
				'log_source_type_id':'Log Source Type',
				'qid':'QID',
				'log_source_id':'Log Source',
				'low_level_category_id':'Low Level Category'
			},
			'config/flow_sources/custom_properties/property_expressions':{
				'qid':'QID',
				'low_level_category_id':'Low Level Category'
			},
			'config/network_hierarchy/networks':{ 'domain_id':'Domain' },
			'siem/offenses':{ 'closing_reason_id':'Closing Reason' }
		}

	def parse(self):
		"""
		Convert JSON file to CSV
		"""
		info('Converting {0} to CSV format'.format(self.input.name))
		if self.endpoint in self.special:
			debug('Endpoint {0} is a special case'.format(self.endpoint))
			self.func = self.special[self.endpoint]()
		elif self.endpoint in self.replacements:
			debug('Replacing some fields for endpoint {0}'.format(self.endpoint))
			self.replace(self.endpoint)
		else:
			debug('Using default formatting for endpoint {0}'.format(self.endpoint))
			self.default()
		info('Output written to {0}'.format(self.output.name))

	def get_fields(self):
		"""
		Get all the fields present in the input values
		This is necessery to avoid misaligned columns when converting to CSV because all items in the input do not have the same fields
		"""
		fields = []
		for item in self.results:
			for field in item:
				if field not in fields:
					fields.append(field)
		return fields

	def write(self, line):
		try:
			self.output.write(line)
		except Exception as e:
			error("Couldn't write line to output file: {0}".format(line))

	def default(self):
		"""
		Default conversion routine.
		Write the values to the CSV file as they appear in the JSON file
		"""
		fields = self.get_fields()
		self.write(','.join(map(self.format_field_name,fields)))

		for item in self.results:
			line = self.make_line(item, fields)
			self.write(line)

	def replace(self, endpoint):
		"""
		Does the same thing as default conversion routine, but rename some fields depending on the endpoint
		"""
		fields = self.get_fields()
		newFields = []
		replacements = self.replacements[endpoint]

		for field in fields:
			newFields.append(replacements.get(field, self.format_field_name(field)))
		self.write(','.join(newFields))

		for item in self.results:
			line = self.make_line(item, fields)
			self.write(line)

	def make_line(self, item, fields):
		"""
		Produce CSV line corresponding to item, using the fields list
		"""
		line = []
		for field in fields:
			value = item.get(field, 'null')
			if value == 'null':
				line.append(value)
				continue
			elif field[-5:] == '_date' or field[-5:] == '_time':
				#If it's a date or time kind of field, convert it to a human readable format
				date = value
				line.append(time.strftime('%d %b %Y %H:%M:%S',time.localtime(date/1000)))
			else:
				line.append(value.encode('utf-8') if type(value) == unicode else value)
		# Enclose all fields in double quotes, and escape inner double quotes
		line = map(lambda x:'"{0}"'.format(str(x).replace('"','""')),line)
		line = '\n' + ','.join(line)
		return line

	def format_field_name(self,field):
		"""
		Format field name from "word1_word2_word3" format to "Word1 Word2 Word3"
		"""
		field = field.encode('utf-8')
		field = field.replace('_', ' ')
		words = field.split(' ')
		words = map(str.capitalize, words)
		return ' '.join(words)

	def event_properties(self):
		"""
		Handle the config/event_sources/custom_properties/property_expressions andpoint
		"""
		global regexPropertyFieldCache

		regexps = self.results

		self.write('Name,Regular Expression,Capture Group,Enabled,Auto-discovered,Use For Rule Engine,Log Source Type,Log Source,Event Name')
		for regex in regexps:
			#Get direct values
			regex_property_identifier = regex['regex_property_identifier']
			expression = regex['regex']
			captureGroup = regex['capture_group']
			enabled = regex['enabled']
			log_source_type_id = regex['log_source_type_id']
			autoDiscovered = ''
			useInRules = ''
			#author = regex['username']

			# Get indirect values
			key = '{0}_{1}'.format(regex_property_identifier, 'name')
			if  key in regexPropertyFieldCache:
				name = regexPropertyFieldCache[key]
				autoDiscovered = regexPropertyFieldCache['{0}_{1}'.format(regex_property_identifier, 'auto_discovered')]
				useInRules = regexPropertyFieldCache['{0}_{1}'.format(regex_property_identifier, 'use_for_rule_engine')]
			else:
				# Get name for property expression from regex_properties file(s)
				name = 'null'
				if 'config/event_sources/custom_properties/regex_properties' in self.extractedFiles:
					found = False
					for regexPropertyFile in self.extractedFiles['config/event_sources/custom_properties/regex_properties']:
						with open(regexPropertyFile) as f:
							regexProperties = json.load(f)
							for regexProperty in regexProperties:
								if regex_property_identifier == regexProperty['identifier']:
									name = regexProperty['name']
									autoDiscovered = regexProperty['auto_discovered']
									useInRules = regexProperty['use_for_rule_engine']
									regexPropertyFieldCache['{0}_{1}'.format(regex_property_identifier, 'name')] = name
									regexPropertyFieldCache['{0}_{1}'.format(regex_property_identifier, 'auto_discovered')] = autoDiscovered
									regexPropertyFieldCache['{0}_{1}'.format(regex_property_identifier, 'use_for_rule_engine')] = useInRules
									found = True
									break
						if found:
							break
					if not found:
						error('No corresponding regex property found for {0}'.format(expression))
				else:
					error('No regex property file found')
					debug(self.extractedFiles)
					break

			self.write('\n"{name}","{expr}","{capt}","{enable}","{autodisc}","{use}","{devtype}","{device}","{event}"'.format(name=name.encode('utf-8'),expr=expression.replace('"','""').encode('utf-8'),capt=captureGroup,enable=enabled,autodisc=autoDiscovered,use=useInRules,devtype=regex['log_source_type_id'],device=regex['log_source_id'],event=regex['qid']))

	def offenses(self):
		global ruleCache

		offenseList = self.results
		self.write('ID,Description,Rules,Event Count,Flow Count,Inactive,Protected,Start Time,Magnitude,Last Updated Time,Status')
		for offense in offenseList:
			offenseID = offense['id']
			description = offense['description']
			ruleIds = [ rule['id'] for rule in offense['rules'] ]
			eventCount = offense['event_count']
			flowCount = offense['flow_count']
			inactive = offense['inactive']
			protected = offense['protected']
			startTime = offense['start_time']
			magnitude = offense['magnitude']
			lastUpdateTime = offense['last_updated_time']
			status = offense['status']
			rules = []
			for ruleId in ruleIds:
				if ruleId in ruleCache.keys():
					rules.append(ruleCache[ruleId])
				else:
					if 'analytics/rules' in self.extractedFiles:
						found = False
						for rulesFile in self.extractedFiles['analytics/rules']:
							f = open(rulesFile)
							rulesList = json.load(f)
							f.close()
							for rule in rulesList:
								if ruleId == rule['id']:
									rules.append(rule['name'])
									ruleCache[ruleId] = rule['name']
									break
			rules = [ rule.replace('"','""') for rule in rules ]
			self.write('\n"{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}","{8}","{9}","{10}"'.format(offenseID,description.replace('"','""').replace('\n',' '),'\t'.join(rules),eventCount,flowCount,inactive,protected,startTime,magnitude,lastUpdateTime,status))

