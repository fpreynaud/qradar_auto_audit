#!/usr/bin/python

import argparse
import re
import time

parser = argparse.ArgumentParser()
parser.add_argument('input', nargs='*',type=str, help='Input file')
args = parser.parse_args()

def parse_licenses():
	licenses = {}
	with open('licenses.csv') as f:
		for line in f:
			hostname,epsLicense,fpmLicense = line.strip().split(',')
			licenses[hostname.split('.')[0].lower()] = {'EPS':epsLicense, 'FPM':fpmLicense}
	return licenses
		

def parse_eps(s):
	epsLast60s = int(re.search(r'\(60s: (\d+)',s).group(1))
	peakEpsLast60s = int(re.search(r'Peak in the last 60s: (\d+)',s).group(1))
	#Problem with year, here
	timestamp = re.search(r'^(\S+\s+\d+\s+\S+)', s).group(1)
	date = timestamp
	timestamp = str(time.localtime().tm_year) + ' ' + timestamp
	timestamp = time.strptime(timestamp,"%Y %b %d %H:%M:%S") 
	timestamp = int(time.mktime(timestamp))
	return (timestamp,date,epsLast60s, peakEpsLast60s)

def parse_fps(s):
	fpsLast60s = int(re.search(r'\[60s: \((\d+)',s).group(1))
	peakFpsLast60s = int(re.search(r'Peak in the last 60s: \((\d+)',s).group(1))
	#Problem with year, here
	timestamp = re.search(r'^(\S+\s+\d+\s+\S+)', s).group(1)
	date = timestamp
	timestamp = str(time.localtime().tm_year) + ' ' + timestamp
	timestamp = time.strptime(timestamp,"%Y %b %d %H:%M:%S")
	timestamp = int(time.mktime(timestamp))
	return (timestamp, date, fpsLast60s, peakFpsLast60s)

licenses = parse_licenses()
for path in args.input:
	m = re.search(r'[ef]ps_(.*).log$',path)
	host = ''
	if m:
		host = m.group(1).lower()
		print(host)
	with open(path) as f:
		with open(path+'.csv','w') as output:
			print('\x1b[32m{0}\x1b[0m'.format(f.name))
			s = f.readline()
			if re.match(r'.*Incoming raw event rate', s):
				output.write('Timestamp,Time,EPS Last 60 Seconds,Peak EPS Last 60 Seconds,License')
				license = ''
				if host != '':
					license = licenses[host]['EPS']
				while s != '':
					timestamp,date,epsLast60s,peakEpsLast60s = parse_eps(s)
					output.write('\n{0},{1},{2},{3},{4}'.format(timestamp,date,epsLast60s,peakEpsLast60s,license))
					s = f.readline()
			elif re.match(r'.*Incoming flow rate', s):
				output.write('Timestamp,Time,FPM Last 60 Seconds,Peak FPM Last 60 Seconds,License')
				license = ''
				if host != '':
					license = licenses[host]['FPM']
				while s != '':
					timestamp,date,fpsLast60s,peakFpsLast60s = parse_fps(s)
					output.write('\n{0},{1},{2},{3},{4}'.format(timestamp,date,fpsLast60s*60,peakFpsLast60s*60,license))
					s = f.readline()
